import React,{useState,useEffect} from 'react'
import { Slide } from 'react-slideshow-image';
import { Bounce } from "react-activity";
import { BrowserRouter as Router , Switch ,Route } from 'react-router-dom';
import 'react-slideshow-image/dist/styles.css';

import Background1 from './images/shopee1.png';
import Background2 from './images/shopee2.png';
import Background3 from './images/shopee3.png';
import css from './mystyle.module.css';

import { Products , NavBar ,Cart , Checkout } from './components';
import ProductDetail from './components/ProductDetail/ProductDetail';
import { commerce } from  './lib/commerce';

const App = () => {
    const [autoplay,setAutoplay] = useState(true);
    
    const [products,setProducts] = useState([]);
    const [cart,setCart] = useState({});
    const [order,setOrder] = useState({});
    const [errorMessage,setErrorMessage] = useState('');

    const fetchProducts = async () =>{
        const { data } = await commerce.products.list();
        // console.log("data:",data)
        setProducts(data)
    } 

    const fetchCart = async() =>{
      const cart = await commerce.cart.retrieve();
      setCart(cart);
    }

    const handleAddToCart = async (productID,quantity) =>{
        const item = await commerce.cart.add(productID,quantity)
        setCart(item.cart)
        // console.log("Cart:",cart)
    }

    const handleUpdateQTY = async(productID,quantity) => {
        //quantity below is cover by {} due to only it update
        const  { cart } = await commerce.cart.update(productID,{quantity})
        setCart(cart)
    }

    const handleRemoveProduct = async(productID) =>{
        const { cart } = await commerce.cart.remove(productID)
        setCart(cart)
    }

    const handleEmptyCart = async () =>{
        const { cart } = await commerce.cart.empty()
        setCart(cart)
    }

    const refreshCart = async() =>{
       const newCart = await commerce.cart.refresh();
       setCart(newCart);
    }
    console.log("Refresh:",cart)
    //Last method to process the payment 
    const handleOrderCaptureCheckout = async(checkoutTokenID,newOrder)=>{
        try{
            const order = await commerce.checkout.capture(checkoutTokenID,newOrder);
            setOrder(order);
            refreshCart();
        }catch(error){
            setErrorMessage(error.data.error.message)
        }
    }

    useEffect(()=>{
        fetchProducts()
        fetchCart()
        
    },[])
    
    // console.log(products)
    const slideProperties = {
        duration: 3000,
        canSwipe: true,
        indicators:true,
        arrows:false,
    };
    
    // const style = {
    //         imgCtner: {
    //             alignItems: 'stretch',
    //             flex: 1,
    //         },
    //         img: {
    //             flex: 1
    //         }
    // }
    return (
        <Router>
            <div style={{backgroundColor:"rgb(245,245,245)"}}>
            <main style={{
                        flexGrow: 1,
                        paddingTop: "5%",
                        paddingLeft: "5%",
                        paddingRight: "5%",
                    }}>
                        <NavBar cart={cart}/>
                        <Switch>
                            <Route exact path="/">
                                <Slide easing="ease" {...slideProperties}>
                                    <div className="each-slide">
                                        <div style={{'backgroundImage': `url(${Background1})`,height:"500px" ,backgroundSize:"100% 100%",backgroundRepeat:"no-repeat"}}/>
                                    </div>
                                    <div className="each-slide">
                                        <div style={{'backgroundImage': `url(${Background2})`,height:500,backgroundSize:"100% 100%",backgroundRepeat:"no-repeat"}}/>
                                    </div>
                                    <div className="each-slide">
                                        <div style={{'backgroundImage': `url(${Background3})`,height:500,backgroundSize:"100% 100%",backgroundRepeat:"no-repeat"}}/>
                                    </div>
                                </Slide>
                                {(products.length===0) ?(<Bounce style={{justifyContent:"center", display: 'flex'}} color="#727981" size={32} speed={1} animating={true} />) :(<Products products={products} onAddToCart={handleAddToCart}/>)}
                            </Route>
                            <Route exact path="/product/:id" component={ProductDetail}/>
                            <Route exact path="/cart">
                                <Cart 
                                    cart={cart}
                                    onUpdateQTY={handleUpdateQTY}
                                    onRemoveProduct={handleRemoveProduct}
                                    onEmptyCart={handleEmptyCart}/>
                            </Route>
                            <Route exact path="/checkout">
                                <Checkout 
                                    cart={cart}
                                    order={order}
                                    onCaptureCheckout={handleOrderCaptureCheckout}
                                    error={errorMessage} 
                                />
                            </Route>
                        </Switch>
            </main>
            </div>
        </Router>
    )
}

export default App
