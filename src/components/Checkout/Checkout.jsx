import React, { useState,useEffect } from 'react'
import { Paper, Stepper, Step, StepLabel, Typography, CircularProgress, Divider, Button } from '@material-ui/core';

import useStyle from './styles';
import AddressForm from './AddressForm';
import PaymentForm from './PaymentForm';

import { Link , useHistory } from 'react-router-dom';
import { commerce } from '../../lib/commerce';

const steps = ['Shipping Address','Payment Detail'];

const Checkout = ({cart,order,onCaptureCheckout,error}) => {
    const [activeStep,setActiveStep] = useState(0)
    const [checkoutToken,setCheckoutToken] = useState(null)
    const classes = useStyle();
    const [shippingData,setShippingData] = useState({});
    const history = useHistory();

    useEffect (()=>{
        const generateToken = async () =>{
            try{
               const token = await commerce.checkout.generateToken(cart.id,{type:'cart'})
               console.log("Checkout_Token:",token)
               setCheckoutToken(token)
            }catch(error){
                if (activeStep !== steps.length) history.push('/');
            }
        }
        generateToken()
    },[cart])

    const nextStep = () => setActiveStep((prevStep)=>prevStep+1)
    const backStep = () => setActiveStep((prevStep)=>prevStep-1)

    const next = (data) =>{
        setShippingData(data)
        nextStep()
        // console.log("Fxxk:",shippingData)
    }

    let Confirmation = () => order.customer ? (
        <div>
            <Typography variant="h5" >Thank you for your purchase ,{order.customer.firstname} {order.customer.lastname}</Typography>
            <Divider className={classes.divider}/>
            <Typography variant="subtitle2">Order ref: {order.customer_reference}</Typography>
            <br />
            <Button type="button" variant="outlined" component={Link} to="/">Back to Home</Button>
        </div>
    ):(
        <div className={classes.spinner}>
            <CircularProgress/>
        </div>
    )
    
    if(error){
        <div>
            <Typography variant="h5">
                Error: {error}
            </Typography>
            <br />
            <Button type="button" variant="outlined" component={Link} to="/">Back to Home</Button>
        </div>
    }

    const Form = () => activeStep == 0 
                ? <AddressForm checkoutToken={checkoutToken} next={next}/>
                : <PaymentForm shippingData={shippingData} checkoutToken={checkoutToken} backStep={backStep} onCaptureCheckout={onCaptureCheckout} nextStep={nextStep}/>

    console.log("ActiveStep:",activeStep)
    console.log("length:",steps.length)

    return (
        <div style={{backgroundColor:"white",marginTop:"-5%"}}>
            {/* <div className={classes.toolbar}/> */}
            <main className={classes.layout}>
                <Paper className={classes.paper}>
                    <Typography variant="h4" align="center" > Checkout </Typography>
                    <Stepper activeStep={activeStep} className={classes.stepper}>
                        {steps.map((step)=>(
                            <Step key={step}>
                                <StepLabel>{step}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    {activeStep == steps.length ? <Confirmation /> : <Form />}
                </Paper>
            </main>
        </div>
    )
}

export default Checkout
