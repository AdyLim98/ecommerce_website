import React,{ useState, useEffect } from 'react'
import { InputLabel ,Select ,MenuItem ,Button ,Grid ,Typography} from '@material-ui/core';
import { useForm , FormProvider } from 'react-hook-form';

import {Link} from 'react-router-dom';
import FormInput from './CustomTextField';
import { commerce } from '../../lib/commerce';
import { Bounce } from "react-activity";

const AddressForm = ({checkoutToken,next}) => {
    //to get all the useForm hook function method
    const methods = useForm ();

    const [shippingCountries,setShippingCountries] = useState([]);
    const [shippingCountry,setShippingCountry] = useState('');
    const [shippingSubdivisions,setShippingSubdivisions] = useState([]);
    const [shippingSubdivision,setShippingSubdivision] = useState('');
    const [shippingOptions,setShippingOptions] = useState([]);
    const [shippingOption,setShippingOption] = useState('');

    const countries =  (Object.entries(shippingCountries).map(([code , name])=>({
        id:code,
        countryName:name,
    })))
    console.log("Countries:",countries)

    const subdivisions =  (Object.entries(shippingSubdivisions).map(([code , name])=>({
        id:code,
        countryName:name,
    })))
    console.log("Subdivisions:",subdivisions)
    
    const options = shippingOptions.map((sO)=>({ id:sO.id , label:`${sO.description} - (${sO.price.formatted_with_symbol})`}));

    const fetchShippingCountries = async (checkoutTokenID) =>{
        const {countries} = await commerce.services.localeListShippingCountries(checkoutTokenID);
        setShippingCountries(countries);
        //convert the object data into array form and get the first one
        setShippingCountry(Object.keys(countries)[0])
        console.log(Object.keys(countries))
        //Object.entries (will give you key ,value)
    }
    //method below is follow the method above ,so cannot put them in 1 same useEffect
    const fetchShippingSubdivisions = async (countryCode) =>{
        const { subdivisions } = await commerce.services.localeListSubdivisions(countryCode)
        setShippingSubdivisions(subdivisions)
        setShippingSubdivision(Object.keys(subdivisions)[0])
    }

    const fetchShippingOptions = async (checkoutTokenID ,country ,region=null) =>{
        const options = await commerce.checkout.getShippingOptions(checkoutTokenID,{country,region})
        setShippingOptions(options)
        setShippingOption(options[0].id)
    }

    //if not [] behind ,then it will always render and makes it lag
    useEffect(()=>{
        if(checkoutToken)fetchShippingCountries(checkoutToken.id)
    },[]);

    useEffect(()=>{
        if(shippingCountry)fetchShippingSubdivisions(shippingCountry)
    },[shippingCountry]);

    useEffect(()=>{
        if(shippingSubdivision)fetchShippingOptions(checkoutToken.id,shippingCountry,shippingSubdivision)
    },[shippingSubdivision]);

    return (
        <div>
            <Typography variant="h6"> Shipping Address </Typography>
            {/* spread all the useForm method into FormProvider SO YOU CAN USE -> FORM tag,onSubmit etc... */}
            <FormProvider {...methods}>
                <form onSubmit={methods.handleSubmit((data) => next({...data,shippingCountry,shippingSubdivision,shippingOption}))}>
                    <Grid container spacing={3}>
                        <FormInput name="firstName" label="First Name" required/>
                        <FormInput name="lastName" label="Last Name" required/>
                        <FormInput name="address" label="Address" required/>
                        <FormInput name="email" label="Email" required/>
                        <FormInput name="city" label="City" required/>
                        <FormInput name="zip" label="ZIP /Postal Code" required/>
                        <Grid item xs={12} sm={6}>
                            <InputLabel>Shipping Country</InputLabel>
                            <Select value={shippingCountry} onChange={(e)=>setShippingCountry(e.target.value)} fullWidth>
                                {/* {console.log(Object.entries(shippingCountries))} */}
                                {/* {(Object.entries(shippingCountries).map(([code , name])=>({
                                    id:code,
                                    countryName:name,
                                })))} */}
                                {countries.map((country)=>( 
                                    <MenuItem key={country.id} value={country.id}>
                                        {country.countryName}
                                    </MenuItem>
                                ))}
                            </Select>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <InputLabel>Shipping Subdivisions</InputLabel>
                            <Select value={shippingSubdivision} onChange={(e)=>setShippingSubdivision(e.target.value)} fullWidth>
                                {subdivisions.map((subdivision)=>(
                                    <MenuItem key={subdivision.id} value={subdivision.id}>
                                        {subdivision.countryName}
                                    </MenuItem>
                                ))}
                            </Select>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <InputLabel>Shipping Options</InputLabel>
                            <Select value={shippingOption} onChange={(e)=>setShippingOption(e.target.value)} fullWidth>
                                {options.map((option)=>(
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.label}
                                        </MenuItem>
                                ))}
                            </Select>
                        </Grid>
                    </Grid>
                    <br />
                    <div style={{display:"flex",justifyContent:"space-between"}}>
                        <Button variant="outlined" component={Link} to="/cart"> Back To Cart</Button>
                        <Button variant="contained" type="submit" color="primary">Next</Button>
                    </div>
                </form>
            </FormProvider>
        </div>
    )
}

export default AddressForm
