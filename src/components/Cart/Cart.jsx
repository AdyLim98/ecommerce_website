import React from 'react'
import {Container,Typography,Button,Grid} from '@material-ui/core';
import { Bounce } from "react-activity";
import { Link } from 'react-router-dom';

import CartItem from './CartItem/CartItem';
import useStyle from './styles';

const Cart = ({cart,onUpdateQTY,onRemoveProduct,onEmptyCart}) => {
    const isEmpty = !cart.total_items;
    const classes = useStyle()

    const EmptyCart = () =>(
        <Typography variant="subtitle1"> You have no items inside your cart , Go <Link to="/" className={classes.link}>Home Page</Link> Adding Some !</Typography>
    )

    const FilledCart = () =>(
        <div>
            <Grid container spacing={3}>
                {cart.line_items.map((item)=>(
                    <Grid item key={item.id} xs={12} sm={4}>
                        <CartItem item={item} onUpdateQTY={onUpdateQTY}
                                    onRemoveProduct={onRemoveProduct}/>
                    </Grid>
                ))}
                <div className={classes.cardDetails}>
                    <Typography variant="h4">
                        Subtotal: {cart.subtotal.formatted_with_symbol}
                    </Typography>
                    <div>
                        <Button className={classes.emptyButton} color="secondary" type="button" variant="contained" size="large" onClick={onEmptyCart}>Empty Cart</Button>
                        <Button component={Link} to="/checkout" className={classes.checkoutButton} color="primary" type="button" variant="contained" size="large">Checkout</Button>
                    </div>
                </div>
            </Grid>
        </div>    
    )
    // if (isEmpty) return (<Bounce style={{justifyContent:"center", display: 'flex'}} color="#727981" size={32} speed={1} animating={true} />); 
    return (
            <Container>
                <div className={classes.toolbar}/>
                <Typography variant="h3" className={classes.title} gutterBottom>Your Shipping Cart</Typography>
                {isEmpty? <EmptyCart /> : <FilledCart />}
            </Container>
      
    )
}

export default Cart
