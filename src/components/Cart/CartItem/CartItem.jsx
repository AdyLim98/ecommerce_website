import React from 'react'
import {Card,CardMedia,CardActions,CardContent,Typography,Button,Grid} from '@material-ui/core';

import useStyle from './styles';
import { Rowing } from '@material-ui/icons';

const CartItem = ({item,onUpdateQTY,onRemoveProduct}) => {
    console.log("Item:",item.id)
    const classes = useStyle();
    return (
        
        <div style={{marginLeft:"5%",marginRight:"5%"}}>
        <Card>
            <CardMedia className={classes.media} image={item.media.source}></CardMedia>
            <CardContent className={classes.cardContent}>
                        <Typography variant="h6" noWrap>  
                                {item.name}
                        </Typography>
                        <Typography variant="body1" style={{paddingTop:"1.5%"}}>  
                                {item.price.formatted_with_symbol}
                        </Typography>   
                </CardContent>
                <CardActions className={classes.cartActions}>
                    <div className={classes.buttons}>
                        <Button type="button" size="small" onClick={()=>onUpdateQTY(item.id,item.quantity-1)}>-</Button>
                        <Typography>{item.quantity}</Typography>
                        <Button type="button" size="small" onClick={()=>onUpdateQTY(item.id,item.quantity+1)}>+</Button>
                    </div>
                    <Button variant="contained" type="button" color="secondary" onClick={()=>onRemoveProduct(item.id)}>Remove</Button>
                </CardActions>
        </Card>
        </div>
    )
}

export default CartItem
