import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    paddingLeft: "10%",
    paddingRight: "10%",
   
  },
  root: {
    flexGrow: 1,
  },
}));