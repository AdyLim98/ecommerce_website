import React from 'react'
import {Card,CardMedia,CardContent,CardActions,Typography,IconButton} from '@material-ui/core';
import {ShoppingCart} from '@material-ui/icons';
import {Link} from 'react-router-dom';
import useStyles from "./styles.js"

const Product = ({product,onAddToCart}) => {
    const classes = useStyles();
    
    const styles = theme => ({
        Card: {
          width: 300,
          margin: 'auto'
        },
        Media: {
          height: 550,
          width: '100%',
          objectFit: 'cover'
        }
    });

    return (
        <div>
            <Card className={classes.root}>
                <CardMedia className={classes.media} image={product.media.source} component={Link} to={`/product/${product.id}`} style={{textDecoration:"none"}}/>
                <CardContent>
                    <div className={classes.cardContent}>
                    {/* Typography use for text ,gutterBottom make it have space bottom */}
                        <Typography variant="h6" gutterBottom noWrap >  
                                {product.name}
                        </Typography>
                        <Typography variant="body1" gutterBottom style={{paddingTop:"1.5%"}}>  
                                {product.price.formatted_with_symbol}
                        </Typography>
                    </div>
                    <Typography noWrap variant="body2" color="textSecondary" dangerouslySetInnerHTML={ {__html: product.description} } className={classes.descriptionPosition}/>
                </CardContent>
                <CardActions className={classes.cardActions}>
                    <IconButton onClick={()=>onAddToCart(product.id,1)}>
                        <ShoppingCart/>
                    </IconButton>
                </CardActions>
            </Card>
        </div>
    )
}

export default Product
